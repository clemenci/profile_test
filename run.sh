#!/bin/bash

for t in PROFGEN PROFUSE Release ; do 
    rm -rf $t
    mkdir -p $t
    (cd $t ; cmake -DCMAKE_BUILD_TYPE=$t ..)
done

make -C PROFGEN
time PROFGEN/test

find PROFGEN -name \*.gcda | sed 's/PROFGEN\(.*\)/cp -v PROFGEN\1 PROFUSE\1/' | sh

make -C PROFUSE
time PROFUSE/test

make -C Release
time Release/test

objdump --disassemble --demangle PROFUSE/CMakeFiles/mylib.dir/mylib.cpp.o > PROFUSE/mylib.asm
objdump --disassemble --demangle Release/CMakeFiles/mylib.dir/mylib.cpp.o > Release/mylib.asm
