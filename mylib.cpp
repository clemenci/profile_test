#include "mylib.h"

#include <iostream>
extern int error;
extern int cnt;

void function() 
{
  int c = 0;

  for (int i = 0; i < cnt; ++i) {
    if (error) {
      std::cout << "got error!" << std::endl;
      return;
    }
    c++;
  }
  std::cout << "counter = " << c << std::endl;
}
